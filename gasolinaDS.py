import pandas as pd
import glob
import os

# Busca o path para todos os .csv da pasta dados
all_files = glob.glob(os.path.join('dados', "*.csv"))

# Cria uma lista para receber os dataframes
li = []

# Itera sobre os arquivos .csv
for filename in all_files:
    ##### Clean #####
    # Lê cada .csv e retorna um dataframe
    # Valores faltantes (['Valor de Compra])
    # Remove as colunas que não estão no escopo da análise
    df = pd.read_csv(filename, header=0, 
                         delimiter=r"\s{2,}",
                         na_values="R$ / litro",
                         usecols=[1, 5, 6, 7, 8],
                         encoding='ISO-8859-1',
                         decimal=',')
    
    # Ajustando os dados faltantes no dataset
    # Quando não tem ['Valor de Compra'] o dataframe coloca o ['Valor de Venda']
    # na coluna do ['Valor de Compra']
    df['Valor de Venda'].fillna(df['Valor de Compra'], inplace=True)
    del df['Valor de Compra']

    # Filta os estados diferentes de RJ e produtos diferentes de "GASOLINA"
    df = df[(df['Estado - Sigla'] == 'RJ') & (df['Produto'] == 'GASOLINA')]
    
    # Transforma as datas em string para datetime64[ns]
    df['Data da Coleta'] = pd.to_datetime(df['Data da Coleta'], dayfirst=True, infer_datetime_format=True)
    del df['Estado - Sigla'], df['Produto']

    # Adiciona a lista de dataframes
    li.append(df)

##### Transformations #####
# Concatena todos os dataframes   
frame = pd.concat(li, axis=0, ignore_index=True)

# Coloca as datas como index
frame.set_index('Data da Coleta', inplace=True)

# Coloca os valores de ['Valor de Venda'] como float
frame['Valor de Venda'] = frame['Valor de Venda'].apply(pd.to_numeric)

##### EDA ##### 
frame.info()

# Calcula a média anual da GASOLINA no RJ
mediaAnual = frame.resample('Y').mean()
print(mediaAnual)

##### View #####
# Plota o gráfico da média anual da GASOLINA no RJ
mediaAnual.plot()
